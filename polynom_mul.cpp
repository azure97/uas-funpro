#include <iostream>
#include <vector>
using namespace std;

vector<int> read_polynomial() {
    int n;
    vector<int> v;

    cout << "Polynomial size: ";
    cin >> n;

    cout << "Polynomial: ";
    v.resize(n);

    for(int &num : v) {
        cin >> num;
    }

    return v;
}

vector<int> polynomial_multiplication(vector<int> s, vector<int> t) {
    vector<int> result(s.size() + t.size() - 1, 0);

    for(int i = 0 ; i < s.size() ; i++) {
        for(int j = 0 ; j < t.size() ; j++) {
            result[i + j] += s[i] * t[j];
        }
    }

    return result;
}

void output_polynomial(vector<int> p) {
    cout << "Polynomial: ";

    for(int num : p) {
        cout << num << " ";
    }

    cout << endl;
}

int main() {
    vector<int> s = read_polynomial();
    vector<int> t = read_polynomial();
    vector<int> result = polynomial_multiplication(s, t);

    output_polynomial(result);
    return 0;
}
