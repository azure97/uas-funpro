-module(polynom_mul).
-export([multiply_polynomial/2]).

add_polynomial(Result, S, []) ->
    lists:reverse(Result) ++ S;

add_polynomial(Result, [], T) ->
    lists:reverse(Result) ++ T;

add_polynomial(Result, [A|S], [B|T]) ->
    add_polynomial([A+B | Result], S, T).

add_polynomial(S, T) ->
    add_polynomial([], S, T).

multiply(Lead, Num, Poly) ->
    Lead ++ [Num * X || X <- Poly].

multiply_polynomial(Result, _, _, []) ->
    Result;

multiply_polynomial(Result, Lead, A, [H|B]) ->
    multiply_polynomial(add_polynomial(Result, multiply(Lead, H, A)), [0|Lead], A, B).

multiply_polynomial(A, B) ->
    multiply_polynomial([0], [], A, B).
