# Polynomial Multiplication #

Simple programs to calculate polynomial multiplication.

## Compile ##

```bash
erlc polynom_mul.erl
g++ -std=c++11 -Wall polynom_mul.cpp -o polynom_mul

```

## Run Program ##

```bash
# running erlang
erl polynom_mul.erl

# running C++
./polynom_mul

```

## Example Erlang Usage ##

```erlang
1> c(polynom_mul).   
{ok,polynom_mul}
2> polynom_mul:multiply_polynomial([1, 3, 4], [5, 10, 9, 100, 20]).
[5,25,59,167,356,460,80]

```

## Example C++ Usage ##

```bash
./polynom_mul
Polynomial size: 3
Polynomial: 1 3 4
Polynomial size: 5
Polynomial: 5 10 9 100 20
Polynomial: 5 25 59 167 356 460 80

```